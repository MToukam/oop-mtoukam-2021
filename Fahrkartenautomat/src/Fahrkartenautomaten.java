﻿import java.util.Scanner;

class Fahrkartenautomaten
{
    public static void main(String[] args)
    { 
    	
    	for (int i = 0 ; ;i++) {
    		if(i >= 1) {
    			System.out.println();
    		}
    		System.out.println("Fahrkartenbestellvorgang:");
    		System.out.println("=========================");
    		
    	    int zuZahlenderBetrag; 
    	    int rückgabebetrag;

    	    zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	    rückgabebetrag    = fahrkartenBezahlen(zuZahlenderBetrag);
    	       
    	    fahrkartenAusgeben();
    	       
    	    rueckgeldAusgeben(rückgabebetrag);
    	}

    }
    
	   public static int fahrkartenbestellungErfassen() {
		   
		   Scanner tastatur = new Scanner(System.in);
		   
		   int zuZahlenderBetrag = 0;
		   int endbetrag;
		   int[] fahrkartenbg  = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};   			   // Fahrkartenbezeichnung
		   int[] fahrkartenprs = {290, 330, 360, 190, 860,900,960,2350,2430,2490}; // Fahrkartenpreis in Cent
		   
		   System.out.println("Wählen Sie bitte Ihre Wunschfahrkarte aus: ");
		   System.out.printf( "  Einzelfahrschein Berlin AB  %8s[2,90  EUR] Auswahl 1\n", " ");
		   System.out.printf( "  Einzelfahrschein Berlin BC  %8s[3,30  EUR] ....... 2\n", " ");
		   System.out.printf( "  Einzelfahrschein Berlin ABC %8s[3,60  EUR] ....... 3\n", " ");
		   System.out.printf( "  Kurzstrecke            %13s[1,90  EUR] ....... 4\n", " ");
		   System.out.printf( "  Tageskarte Berlin AB   %13s[8,60  EUR] ....... 5\n", " ");
		   System.out.printf( "  Tageskarte Berlin BC   %13s[9,00  EUR] ....... 6\n", " ");
		   System.out.printf( "  Tageskarte Berlin ABC  %13s[9,60  EUR] ....... 7\n", " ");
		   System.out.println("  Kleingruppen-Tageskarte Berlin AB   [23,50 EUR] ....... 8");
		   System.out.println("  Kleingruppen-Tageskarte Berlin BC   [24,30 EUR] ....... 9");
		   System.out.println("  Kleingruppen-Tageskarte Bewrlin ABC [24,90 EUR] ....... 10\n");

		   
		   System.out.print("Ihre Wahl: ");    // Eingabe einer Auswahl, siehe Array Fahrkartenbezeichung
		   int auswahl = tastatur.nextInt();	
		   
		   while(auswahl > fahrkartenbg.length) {			// Ist diese Auswahl größer als
			   System.out.println(" >>falsche Eingabe<<");	// die Zahlen, die daer Array liefern kann,
  		       System.out.print("Ihre Wahl: ");				// dann wird das als eine falsche eingabe gewertet,
  		       auswahl = tastatur.nextInt();				// und es muss noch eine Auswahl getroffen werden, und
		   }												// der Kaufvorgang geht nur dann weiter, wenn eine richtige Auswahl eingegeben ist.
		   
		   for(int i = 0; i < fahrkartenbg.length; i++) {
				if(auswahl == fahrkartenbg[i]) zuZahlenderBetrag = fahrkartenprs[i]; 
		   }

		   System.out.print("Anzahl der Tickets: ");
		   int anzahlTickets = tastatur.nextInt();
		    
		   while((anzahlTickets < 1) || (anzahlTickets > 10)) {
			   System.out.println("Falshe Eingabe, bitte nochmal: ");
			   anzahlTickets = tastatur.nextInt();
		   }
		   endbetrag = zuZahlenderBetrag*anzahlTickets;
		 
		   return endbetrag;
	   }
	   
	   public static int fahrkartenBezahlen(int zuZahlenderBetrag) {
		   int eingezahlterGesamtbetrag = 0;
		   int eingeworfeneMünze;
		   Scanner tastatur = new Scanner(System.in);
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f Euro\n",  (double) (zuZahlenderBetrag- eingezahlterGesamtbetrag)/100);
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 5 Euro): ");
	    	   eingeworfeneMünze = (int) (100*tastatur.nextDouble());
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
	       int       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       return rückgabebetrag;
	   }
	   
	   public static void fahrkartenAusgeben() {
	       // Fahrscheinausgabe
	       // -----------------
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          warte(250);
	       }
	       System.out.println("\n\n");
	   }
	   
	   public static void warte(int millisekunde) {
	          try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   }
	   
	   public static void rueckgeldAusgeben(int rückgabebetrag) {
	       if(rückgabebetrag > 0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von  %.2f EURO\n", (double) rückgabebetrag/100 );
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 200) // 2 EURO-Münzen
	           {
	        	  muenzeAusgeben(2, "Euro");
		          rückgabebetrag -= 200;
	           }
	           while(rückgabebetrag >= 100) // 1 EURO-Münzen
	           {
	        	  muenzeAusgeben(1, "Euro");
		          rückgabebetrag -= 100;
	           }
	           while(rückgabebetrag >= 50) // 50 CENT-Münzen
	           {
	        	  muenzeAusgeben(50, "Cent");
		          rückgabebetrag -= 50;
	           }
	           while(rückgabebetrag >= 20) // 20 CENT-Münzen
	           {
	        	  muenzeAusgeben(20, "Cent");
	 	          rückgabebetrag -= 20;
	           }
	           while(rückgabebetrag >= 10) // 10 CENT-Münzen
	           {
	        	  muenzeAusgeben(10, "Cent");
		          rückgabebetrag -= 10;
	           }
	           while(rückgabebetrag >= 5)// 5 CENT-Münzen
	           {
	        	  muenzeAusgeben(5, "Cent");
	 	          rückgabebetrag -= 5;
	           }
	       }

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine schöne Fahrt.");
	   }
	   
	   public static void muenzeAusgeben(int betrag, String einheit) {
		  if(einheit.equals("Euro")) {
			  System.out.printf("%-5.2f %-6s%n", (double) betrag, einheit);
		  }
		  else if (einheit.equals("Cent")){
			  System.out.printf("%-5d %-6s%n", betrag, einheit);
		  }
	   }
}