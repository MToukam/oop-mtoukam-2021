
public class Datum {
	private int tag;
	private int monat;
	private int jahr;
	
	public static void main(String[] args)  throws FalschesDatumEx{
		Datum d1;
		try {
			d1 = new Datum(4, 13, 1981); 
		}
		catch(FalschesDatumEx ex) {
			System.out.print(ex.getMessage());
			System.out.println(ex.getFWert());
		}
		
	}

	public Datum() {
		this.tag = 1;
		this.monat = 1;
		this.jahr = 1970;
	}

	public Datum(int tag, int monat, int jahr) throws FalscherTagEx,  FalscherMonatEx, FalschesJahrEx{
		setTag(tag);
		setMonat(monat);
		setJahr(jahr);
	} 

	public int getTag() {
		return tag;
	}

	public void setTag(int tag) throws FalscherTagEx{
		if(tag < 1 || tag > 31)
			throw new FalscherTagEx("Error: falscher Tag", tag);
		this.tag = tag;
	}

	public int getMonat() {
		return monat;
	}

	public void setMonat(int monat) throws FalscherMonatEx{
		if(monat < 1 || monat > 12)
			throw new FalscherMonatEx("Error: falscher Monat ", monat);
		this.monat = monat;
	}

	public int getJahr() {
		return jahr;
	}

	public void setJahr(int jahr) throws FalschesJahrEx{
		if(jahr < 1900 || jahr > 2100)
			throw new FalschesJahrEx("Error: falsches Jahr ", jahr);
		this.jahr = jahr;
	}

}
