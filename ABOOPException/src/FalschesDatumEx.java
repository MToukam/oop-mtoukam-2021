
public  class FalschesDatumEx extends Exception{
	private int fWert;// falscher Wert
	
	public FalschesDatumEx(String message, int fWert) {
		super(message);
		this.fWert = fWert;
	}
	
	public int getFWert() {
		return this.fWert;
	}
}
