package myUtil;

public class MyIIntArray implements IIntArray{
	
	private int[] zliste;
	
	public MyIIntArray(int l�nge) {
		zliste = new int[l�nge];
	}

	public static void main(String[] args) throws MyIndexException{
		IIntArray zliste = new MyIIntArray(6);
		zliste.increase(4);
		int a ;
		
		try {
			zliste.set(1, 4);
			a = zliste.get(1);
			System.out.println(a);
			zliste.set(9, 2);
			a = zliste.get(9);
			System.out.println(a);
			zliste.set(-12, 3);
			
		}
		catch(MyIndexException ex) {
			System.out.print(ex.getMessage());
			System.out.println(ex.getWrongIndex());	
		}
	}
	
	public int get(int index) throws MyIndexException {
		if(index >= zliste.length || index < 0) {
			throw new MyIndexException(index);
		}
		return zliste[index];
	}
	
	public void set(int index, int value) throws MyIndexException {
		if(index >= zliste.length || index < 0) {
			throw new MyIndexException(index);
		}
		zliste[index] = value;
	}
	
	public void increase(int n) {
		int[] zlisteTemp = zliste;
		zliste = new int[zlisteTemp.length + n];
		for(int i = 0; i < zlisteTemp.length; i++) {
			zliste[i] = zlisteTemp[i];
		}
	}

}
