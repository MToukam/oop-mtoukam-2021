
public class SchleifenBeispiele {

	public static void main(String[] args) {
		
		//i++ und ++i
		
		int i = 5;
		int y = 4 + (i++);
		int x = 4 + (++i);
		System.out.println("y = " + y);
		System.out.println("x = " + x);
		
//		int x = 5;
//		// kopfgesteuerte Schleifen
//		while(x > 2) {
//			System.out.println("x : " + x);
//			x--;
//		}
//		
//		System.out.println("-----------");
//		x = 5;
//		// Fu�gesteuerte Schleifen
//		do {
//			System.out.println("x : " + x);
//			x--;
//		} while(x > 2);
//		
//		// for-Schleife
//		for (int i = 0; i < 10; i++) { // ++i, i = i+1
//			System.out.print(i + " ");
//		}
//		System.out.println();
//		
//		for (int i = 9; i >= 0 ; i--) { // --i, i = i-1
//			System.out.print(i + " ");
//		}
//		System.out.println();
//		
//		for (int i = 0; i < 10; i++) { // ++i, i = i+1
//			System.out.print((9-i) + " ");
//		}
//		System.out.println();
		

	}

}
