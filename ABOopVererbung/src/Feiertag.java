
public class Feiertag extends Datum{
	private String fName;  			// Name des Feiertags
	
	public Feiertag() {
		super();
		fName = "Neujahr";
	}
	public Feiertag(int tag, int monat, int jahr, String fName) {
		super(tag, monat, jahr); // überladene Konstruktor der Klasse Datum wird aufgerufen, (Datum(tag, monat, jahr))
		setfName(fName);
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}
	
	@Override
	public String toString() {
		return super.toString() + " (" + this.fName + ")";
	}
	

}
