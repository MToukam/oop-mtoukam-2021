import java.util.Scanner;

public class Bankkonto {
	
	private String inhaber;
	private int kontonummer;
	private double kontostand;
	
	Bankkonto(){
		inhaber = "M�ller";
		kontonummer = 123;
		kontostand = 0;	
	}
	
	Bankkonto(String inhaber, int kontonummer, double kontostand){
		setInhaber(inhaber);
		setKontonummer(kontonummer);
		setKontostand(kontostand);
		
	}

	public String getInhaber() {
		return inhaber;
	}

	public void setInhaber(String inhaber) {
		this.inhaber = inhaber;
	}

	public int getKontonummer() {
		return kontonummer;
	}
	
	public void setKontonummer(int kontonummer) {
		this.kontonummer = kontonummer;
	}


	public double getKontostand() {
		return kontostand;
	}

	public void setKontostand(double kontostand) {
		this.kontostand = kontostand;
	}
	
	public void einzahlen(double betrag) {
		kontostand += betrag;
	}
	
	public double  auszahlen(double betrag) {
		kontostand -= betrag;
		return betrag;
	}
	
	@Override
	public String toString() {
//		return "Inhaber: " + this.inhaber + ", Kontonummer: " + this.kontonummer + ", neuer Kontostand " + this.kontostand + " �";
		return String.format("Inhaber:  %s , Kontonummer: %d, neuer Kontostand: %.2f� ", this.inhaber, this.kontonummer, this.kontostand);
	}

}
