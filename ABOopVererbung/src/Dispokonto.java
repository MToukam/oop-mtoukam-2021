
public class Dispokonto extends Bankkonto{
	
	private double dispokredit;
	
	public Dispokonto() {
		super();
		dispokredit = 0;
	}
	
	public Dispokonto(String inhaber, int kontonummer, double kontostand, double dispokredit) {
		super(inhaber, kontonummer, kontostand);
		setDispokredit(dispokredit);
	}
	
	public Dispokonto(double dispokredit) {
		setDispokredit(dispokredit);
	}
	

	public double getDispokredit() {
		return dispokredit;
	}

	public void setDispokredit(double dispokredit) {
		this.dispokredit = dispokredit;
	}
	
	@Override
	public double  auszahlen(double betrag) {
		if(betrag <=  (getKontostand() + this.dispokredit)) {
			setKontostand(getKontostand()- betrag);
		}
		else {
			betrag = getKontostand() + this.dispokredit;
			setKontostand(getKontostand()- betrag);
		}
		return betrag;
	}
	
	public String toString() {
		return super.toString() + String.format(", Dispokredit: %.2f�", this.dispokredit);
	}




}
