import java.util.Scanner;

public class MainMethode {

	public static void main(String[] args) {
		
//		Feiertag f1 = new Feiertag(24, 12, 2014, "Heilig Abend");
//		Feiertag f2 = new Feiertag();
//		System.out.println(f1);
//		System.out.println(f2);
		
		
		Bankkonto b1 = new Bankkonto("Will Schweber", 490, -60);
		System.out.println(b1);
		b1.einzahlen(50);
		System.out.println(b1);
		double betrag = b1.auszahlen(50);
		System.out.println(b1);
		
		
		Dispokonto d1 = new Dispokonto("Max M�ller", 237, -60, 100);
		System.out.println(d1);
		double betrag2 = ((Bankkonto)d1).auszahlen(50);
		System.out.println("Sie haben " + betrag2 + " Euro ausgzahlt");
		System.out.println(d1);
		
		Bankkonto[] kliste = {
				new Bankkonto("Marc Schneider", 456, 200),
				new Bankkonto("Marc Schneidermann", 645, 300),
				new Dispokonto("Eric Matthieux", 908, 500, 1000),
				new Dispokonto("Linn Extelle", 781, 10000, 350)
		};
		
		for(int i = 0; i < kliste.length; i++) {
			System.out.println(kliste[i]);
		}

	}

}
