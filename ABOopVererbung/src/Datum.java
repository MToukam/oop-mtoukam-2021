
public class Datum {
	
	// *** Attribute
	private int tag;
	private int monat; 
	private int jahr;
	private static int quartal = 0;// Klassenattribut
	
	// *** Konstruktoren
	public Datum() {
		this.tag = 1;
		this.monat = 1;
		this.jahr = 1970;
	}
	
	public Datum( int tag, int monat, int jahr) {
		setTag(tag);   		// this.tag = tag;
		setMonat(monat);	// this.monat = monat;
		setJahr(jahr);		// this.jahr = jahr;
	}
	
	// *** Methoden
	public int getTag() {
		return tag;
	}
	public void setTag(int tag) {
		if(tag <= 31 && tag > 0)
			this.tag = tag;
	}
	
	public int getMonat() {
		return monat;
	}
	public void setMonat(int monat) {
		if(monat <= 12 && monat > 0)
			this.monat = monat;
	}
	
	public int getJahr() {
		return jahr;
	}
	public void setJahr(int jahr) {
		this.jahr = jahr;
	}
	
	@Override
	public String toString() {
		return this.tag + "." + this.monat + "." + this.jahr;
	}
	
	@Override
	public boolean equals(Object obj) {
			
		if(obj instanceof Datum) {
			Datum d = (Datum) obj;
			if(this.monat == d.getMonat() ) { // nur Monate werden verglichen
				return true;
			}
			else
				return false;
		}
		return false;
	}
	
	public  static int berechneQuartal(Datum dt) {
		int qt = dt.getMonat();
	
		if(qt >= 1 && qt <=4) {
			quartal = 1;
		}
		else if(qt  > 4 && qt  <=8) {
			quartal = 2;
		}
		else if (qt > 8 && qt <=12) {
			quartal = 3;
		}
		return quartal;
	}
	
}
