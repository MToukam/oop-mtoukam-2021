import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {

		// Benutzereingaben lesen
		String artikel = liesString("Was m�chten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}
	
	   public static String liesString(String text){
		   Scanner tastatur = new Scanner(System.in);
		   System.out.println(text);
		   String text2  = tastatur.next();
		   return text2;
	   }
	   
	   public static int liesInt(String text) {
		   Scanner tastatur = new Scanner(System.in);
		   System.out.println(text);
		   int ergebnis  = tastatur.nextInt();
		   return ergebnis;
	   }
	   
	   public static double liesDouble(String text) {
		   Scanner tastatur = new Scanner(System.in);
		   System.out.println(text);
		   double ergebnis  = tastatur.nextDouble();
		   return ergebnis;
	   }
	   
	   public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		   return anzahl * nettopreis;
	   }
	   
	   public static double berechneGesamtbruttopreis(double nettogesamtpreis,double mwst) {
		   return nettogesamtpreis * (1 + (double) mwst / 100);
	   }
	   
	   public static void rechungausgeben(String artikel, int anzahl, double
			   nettogesamtpreis, double bruttogesamtpreis, double mwst) {
			System.out.println("\tRechnung");
			System.out.printf("\t\t Netto:  %-20s %6d %10.2f EUR %n", artikel, anzahl, nettogesamtpreis);
			System.out.printf("\t\t Brutto: %-20s %6d %10.2f EUR (%.1f%s)%n", artikel, anzahl, 
					bruttogesamtpreis, mwst, "%");
	   }


}
