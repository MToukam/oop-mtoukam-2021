
public interface IntStack {
	void push(int a);
	int pop();
} 
