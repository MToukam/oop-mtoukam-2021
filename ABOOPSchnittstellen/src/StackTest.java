
public class StackTest {

	public static void main(String[] args) {
		IntStack s1 = new Stack(2);
		
		s1.push(5);
//		s1.speicherAusgeben();
		System.out.println(s1);
		s1.push(2);
//		s1.speicherAusgeben();
		System.out.println(s1);
		s1.push(7);
//		s1.speicherAusgeben();
		System.out.println(s1);
		
		s1.push(4);
		System.out.println(s1);
		s1.push(6);
		System.out.println(s1);
		s1.push(3);
		System.out.println(s1);
		s1.push(1);
		System.out.println(s1);
		s1.push(1);
		System.out.println(s1);
		s1.push(1);
		System.out.println(s1);
		s1.push(1);
		System.out.println(s1);
		
		s1.pop();
		System.out.println(s1);
		s1.push(7);
		System.out.println(s1);
		s1.pop();
		System.out.println(s1);
		

	}

}
