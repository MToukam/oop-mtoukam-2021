
public class Datum implements Comparable<Datum> {

	// *** Attribute
	private int tag;
	private int monat;
	private int jahr;

	// *** Konstruktoren
	public Datum() {
		this.tag = 1;
		this.monat = 1;
		this.jahr = 1970;
	}

	public Datum(int tag, int monat, int jahr) {
		setTag(tag); // this.tag = tag;
		setMonat(monat); // this.monat = monat;
		setJahr(jahr); // this.jahr = jahr;
	}

	// *** Methoden
	public int getTag() {
		return tag;
	}

	public void setTag(int tag) {
		if (tag <= 31 && tag > 0)
			this.tag = tag;
	}

	public int getMonat() {
		return monat;
	}

	public void setMonat(int monat) {
		if (monat <= 12 && monat > 0)
			this.monat = monat;
	}

	public int getJahr() {
		return jahr;
	}

	public void setJahr(int jahr) {
		this.jahr = jahr;
	}

	@Override
	public boolean equals(Object obj) {
		Datum d = (Datum) obj;
		if (this.tag == d.tag && this.monat == d.monat && this.jahr == d.jahr)
			return true;
		return false;
	}

	@Override
	public String toString() {
		return this.tag + "." + this.monat + "." + this.jahr;
	}

	@Override
	public int compareTo(Datum d) {
		if(this.jahr < d.getJahr())
			return -1;
		if(this.jahr > d.getJahr())
			return 1;
		else {
			if(this.monat < d.getMonat())
				return -1;
			if(this.monat > d.getMonat())
				return 1;
			else {
				if(this.tag < d.getTag())
					return -1;
				if(this.tag > d.getTag())
					return 1;
				else
					return 0;
			}
		}
		
	}
	


}
