
public class Stack implements IntStack{
	private int[] speicher;
	private int index = 0;
	
	public Stack( int arrgr) {
		this.speicher = new int [arrgr];
	}
	
	@Override
	public void push( int b) {
		if(index < this.speicher.length) {
			speicher[index] = b;
		}
		else {
			int[] speicher2 = this.speicher;
			this.speicher = new int [index + 20]; 
			for(int u = 0; u < index ; u++) {
				speicher[u] = speicher2[u];
			}
			speicher[index] = b;	
		}
		index++;	
	};
	
	public int pop() {
		if(index > 0) {
			index--;
			return speicher[index];
		}
		else {
			System.out.println("Speichergrenze erreicht, bitte nicht mehr popen");
			return speicher[index];
		}
		
	}
	
	
	public void speicherAusgeben() {
		System.out.print("[");
		for(int i = 0; i < this.index; i++) {
			if(i == (this.index -1))
				System.out.print(speicher[i]);
			else
				System.out.print(speicher[i] + ",");
		}
		System.out.println("]");
	}
	
	@Override
	public String toString() {
	String ergebnis = "[";
	for(int i = 0; i < this.index; i++) {
		if(i == (this.index -1))
			ergebnis += speicher[i];
		else
			ergebnis += speicher[i]+ ",";
	}
	ergebnis += "]";
	return ergebnis;
	
	}
}
