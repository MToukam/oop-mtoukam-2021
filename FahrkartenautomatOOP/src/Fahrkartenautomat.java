import java.util.Scanner;

public class Fahrkartenautomat {
	private static Ticket[] ticketarten = {new Ticket("Einzelfahrschein Berlin AB", 2.90, 0 ), 
			new Ticket("Einzelfahrschein Berlin BC", 3.30, 0 ),
			new Ticket("Einzelfahrschein Berlin ABC", 3.60, 0 ),
			new Ticket("Kurszstrecke", 1.90, 0 ),
			new Ticket("Tageskarte Berlin AB", 8.60, 0 ),
			new Ticket("Tageskarte Berlin BC", 9.00, 0 ),
			new Ticket("Tageskarte Berlin ABC", 9.60, 0 ),
			new Ticket("Kleingruppen-Tageskarte Berlin AB", 23.50, 0 ),
			new Ticket("Kleingruppen-Tageskarte Berlin BC", 24.30, 0 ),
			new Ticket("Kleingruppen-Tageskarte Berlin ABC", 24.90, 0 )
			};
	
	
	public Fahrkartenautomat(){
		ticketarten = null;
	}
	
	public static void main(String[] args) {
		double zuZahlenderBetrag;
		double r�ckgabebetrag;
		
		do {
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			r�ckgeldAusgeben(r�ckgabebetrag);

			System.out.println("\n  Vergessen Sie nicht, den Fahrschein\n" + "  vor Fahrtantritt entwerten zu lassen!\n"
					+ "  Wir w�nschen Ihnen eine sch�ne Fahrt.\n\n");
		}while(true);
	}
	
	public static double fahrkartenbestellungErfassen() {  
	    double gesamtPreis = 0.0;
	    int auswahl;
	    
	    Scanner tastatur = new Scanner(System.in);

	    System.out.println(" Fahrkartenbestellvorgang:");
	    System.out.println(" =========================");
	     
	     do {
	    	 auswahl = fahrkartenMenue();
	    	 if (auswahl == 0)
	        	  for (int i=0 ; i < ticketarten.length ; i++)
	                  gesamtPreis += ticketarten[i].getPreis() * ticketarten[i].getAnzahl(); // ticketarten[i].getAnzahl
	         else 
	         	 erfasseTicketAnzahl(auswahl);  // erfasseTicketAnzahl(auswahl);  
	     } while(auswahl !=0);

		return gesamtPreis;
	}
	
	public static void erfasseTicketAnzahl(int auswahl) {
		  Scanner tastatur = new Scanner(System.in);
		  System.out.print("\n  Anzahl der Tickets: ");
		  int newAnzahl = ticketarten[auswahl-1].getAnzahl() + tastatur.nextInt();
		  ticketarten[auswahl-1].setAnzahl(newAnzahl);
	}
	
	public static int fahrkartenMenue() {
        int auswahl;
        boolean istEingabeKorrekt = false;
        Scanner tastatur = new Scanner(System.in);
        do {
          System.out.println("\n  W�hlen Sie :");
          for (int i = 0; i < ticketarten.length ; i++)
            System.out.println(ticketarten[i] + " (" + (i+1) + ")"); // Wegen toString 
          // System.out.println("    " + ticketarten[i].getBezeichner()+" ("+ (i+1) + ")");  
          System.out.println("    Bezahlen (0)\n");
          System.out.print("  Ihre Wahl: ");
          auswahl = tastatur.nextInt();
          if (auswahl <= ticketarten.length && auswahl >= 0)
            istEingabeKorrekt = true;
          else
            System.out.println("  >>falsche Eingabe<<");

        } while (!istEingabeKorrekt);
        return auswahl;
     }
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfeneM�nze;
		double eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.format("  Noch zu zahlen: %4.2f � %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("  Eingabe (mind. 5Ct, h�chstens 10 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}
	
	public static void fahrkartenAusgeben() {
		System.out.println("\n  Fahrschein wird ausgegeben");
		System.out.print("  ");
		for (int i = 0; i < 10; i++) {
			System.out.print("=");
			warte(255);
		}
		System.out.println("\n");
	}
	
	public static void warte(int milisekunde) {
		try {
			Thread.sleep(milisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	public static void r�ckgeldAusgeben(double r�ckgabebetrag) {

		if (r�ckgabebetrag > 0.0) {
			System.out.format("  Der R�ckgabebetrag in H�he von %4.2f � %n", r�ckgabebetrag);
			System.out.println("  wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) {// 2 EURO-M�nzen
				m�nzeAusgeben(2, "EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) {// 1 EURO-M�nzen
				m�nzeAusgeben(1, "EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				m�nzeAusgeben(50, "CENT");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				m�nzeAusgeben(20, "CENT");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				m�nzeAusgeben(10, "CENT");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				m�nzeAusgeben(5, "CENT");
				r�ckgabebetrag -= 0.05;
			}
		}
	}

	public static void m�nzeAusgeben(int betrag, String einheit) {

		System.out.println("                 * * *        ");
		System.out.println("               *       *      ");
		System.out.format("              *    %-2s   *     %n", betrag);
		System.out.format("              *   %4s  *     %n", einheit);
		System.out.println("               *       *      ");
		System.out.println("                 * * *        ");

	}

}
