
public class Ticket {
	private String bezeichner;
	private double preis;
	private int anzahl;
	
	// Standardkonstruktor
	public Ticket(){
		super();
	}
	
	// �berladen des Konstruktors Ticket()
	public Ticket(String bezeichner, double preis, int anzahl){
		this.bezeichner = bezeichner;  	// setBezeichner(bezeichner)
		this.preis = preis;				// setPreis(Preis)
		this.anzahl = anzahl;			// setAnzahl(anzahl)
	}
	
	public String getBezeichner() {
		return this.bezeichner;
	}
	
	public double getPreis() {
		return this.preis;
	}
	
	public int getAnzahl() {
		return this.anzahl;
	}
	
	public void setBezeichner(String bezeichner) {
		this.bezeichner = bezeichner;
	}
	
	public void setPreis(double preis) {
		this.preis = preis;
	}
	
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
	@Override
	public String toString() {
		return String.format("    %s  ...% .2f�", this.bezeichner, this.preis);
	}
	


}
