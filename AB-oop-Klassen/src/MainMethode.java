
public class MainMethode {

	public static void main(String[] args) {
		
		Datum d1 = new Datum();
		Datum d2 = new Datum();
		d2.setTag(7);
		d2.setMonat(5);
		d2.setJahr(2021);
		Datum d3 = new Datum(4, 8, 2021);
		Datum d4 = new Datum(5, 12, 2021);
		
		System.out.printf("%02d.", d1.getTag());
		System.out.printf("%02d.", d1.getMonat());
		System.out.println(d1.getJahr());
		
		System.out.printf("%02d.", d2.getTag());
		System.out.printf("%02d.", d2.getMonat());
		System.out.println(d2.getJahr());
		
		System.out.printf("%02d.", d3.getTag());
		System.out.printf("%02d.", d3.getMonat());
		System.out.println(d3.getJahr());
		
		System.out.println(d4); // System.out.println(d4.toString());
		if(d3.equals(d4)) 
			System.out.println("Monat gleich");
		else
			System.out.println("Monat ungleich");
		
		System.out.println("Das Quartal von "+ d1 + " ist " + Datum.berechneQuartal(d1));
		System.out.println("Das Quartal von "+ d2 + " ist " + Datum.berechneQuartal(d2));
		System.out.println("Das Quartal von "+ d3 + " ist " + Datum.berechneQuartal(d3));
		System.out.println("Das Quartal von "+ d4 + " ist " + Datum.berechneQuartal(d4));
		
		
		KomplexeZahl c1 = new KomplexeZahl();
		KomplexeZahl c2 = new KomplexeZahl();
		c2.setRealteil(2);
		c2.setImteil(-3);
		KomplexeZahl c3 = new KomplexeZahl(-4, 1);
		KomplexeZahl c4 = KomplexeZahl.multKomplex(c2, c3);
		KomplexeZahl c5 = c2.multKomplex2(c4);
		
		System.out.print(c1);
		System.out.print(c2);
		System.out.printf("%.1f%+.1fj%n", c3.getRealteil(), c3.getImteil());
		if(c2.equals(c3))
			System.out.println("Real- und Imaginärteil gleich");
		else
			System.out.println("Real- und Imaginärteil ungleich");
		
		
		System.out.print(c4);
		System.out.print(c5);
	
	}

}
