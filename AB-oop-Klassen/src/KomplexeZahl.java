
public class KomplexeZahl {
	
	// **** Attribute
	private double realteil;
	private double imteil;
	
	// **** Konstuktoren
	public KomplexeZahl() {
		this.realteil = 0.0;
		this.imteil   = 0.0;
	}
	
	public KomplexeZahl(double realteil, double imteil) {
		setRealteil(realteil);
		setImteil(imteil);
	}
	
	// **** Methoden
	public double getRealteil() {
		return this.realteil;
	}
	public double getImteil() {
		return this.imteil;
	}
	
	public void setRealteil(double realteil) {
		this.realteil = realteil;
	}
	
	public void setImteil(double imteil) {
		this.imteil = imteil;
	}
	
	// Klassenmethode, statische Methhode, gehört zu der klasse KomplexeZahl
	public static KomplexeZahl multKomplex(KomplexeZahl k1, KomplexeZahl k2) {
		double a, b;
		a = k1.getRealteil() * k2.getRealteil() - k1.getImteil()*k2.getImteil();
		b = k1.getRealteil() * k2.getImteil() + k2.getRealteil()*k1.getImteil();
		return new KomplexeZahl(a, b);
	}
	
	// Objektmethode
	public KomplexeZahl multKomplex2(KomplexeZahl c) {
		double cre, cim;
		cre = c.getRealteil() * this.realteil - c.getImteil()*this.imteil;
		cim = c.getRealteil() * this.imteil + this.realteil*c.getImteil();
		return new KomplexeZahl(cre, cim);
	}
	
	
	// Methoden, die hier überschrieben werden
	@Override
	public String toString() {
//		return this.realteil + " + " + this.imteil +"j";
		return String.format("%.1f%+.1fj %n", this.realteil,  this.imteil);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof KomplexeZahl) {
			KomplexeZahl k = (KomplexeZahl) obj;
			if(this.realteil == k.getRealteil() &&
					this.imteil == k.getImteil()) {
				return true;
			}
			else return false;
		}
		return false;
	}
	
}
