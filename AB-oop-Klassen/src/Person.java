
public class Person {
	private String name;			// Name
	private Datum birth; 	 		// Geburtsdatum, Objekt vom Typ Datum
	private static int anzahl = 0;	// statisches Attribut
	private  int nummer;			// nicht-statisches Attribut
	
	public Person(String name, Datum birth) {
		this.name = name;
		this.birth = birth;
		anzahl++;
	}
	public static int getAnzahl() {
		return anzahl;
	}
	public  void  setNummer(int nummer) {
		this.nummer = nummer;
	}
	public int getNummer() {
		return this.nummer;
	}
	public String getName() {
		return this.name;
	}
	public Datum getBirth() {
		return this.birth;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String toString() {
		return "Name: " + this.name + ", Geburtsdatum: " + this.birth;
	}
	
}
