
public class PersonTest {

	public static void main(String[] args) {
		
	
		Person[] p = {	new Person("Max", 	new Datum(2, 8, 1996)),
						new Person("Willi", new Datum(3, 12, 2000)),
						new Person("Linn", 	new Datum(3, 10, 2000))
					};
		for( int i = 0; i < p.length; i++) {
			p[i].setNummer(i+1);
			System.out.println("Nummer: " + p[i].getNummer() + ", " + p[i]);
		}
		System.out.println("Anzahl der gesamten Personen: " + Person.getAnzahl());
		

	}

}
