import java.util.Scanner;

public class BMIRechner {

	public static void main(String[] args) {
		
		double bmi;
		
		System.out.println("Dieses Programm soll Ihren BMI-Wert berechnen, dafuer werden einige Eingaben von Ihnen benoetigt!");
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie bitte Ihre Körpergroesse in cm ein : ");
		int kgroesse = myScanner.nextInt();
		
		System.out.println("Geben Sie bitte Ihren Gewicht in kg ein : ");
		int gewicht = myScanner.nextInt();
		
		System.out.println("Geben Sie bitte Ihren Geschlecht ein : ");
		char geschlecht = myScanner.next().charAt(0);
		
		double kgroesseInm = (double)kgroesse/100;
		
//		bmi = gewicht/(kgroesseInm*kgroesseInm);
		
		bmi = gewicht/(Math.pow(kgroesseInm, 2));
		
		System.out.printf("Ihr BMI-Wert ist : %.2f%n", bmi);
		
		boolean untergewichtFrau  = (geschlecht =='w') && (bmi < 19);
		boolean untergewichtMann  = (geschlecht =='m') && (bmi < 20);
		
		boolean normalgewichtFrau = (geschlecht =='w') && (bmi <= 24 && bmi >=19);
		boolean normalgewichtMann = (geschlecht =='m') && (bmi <= 25 && bmi >=20);
		
		boolean uebergewichtFrau  = (geschlecht =='w') && (bmi > 24);
		boolean uebergewichtMann  = (geschlecht =='m') && (bmi > 25);
		
		if(untergewichtFrau  || untergewichtMann){
				System.out.println("Klassifikation : Untergewicht");
		}
		
		if(normalgewichtFrau || normalgewichtMann){
				System.out.println("Klassifikation : Normalgewicht");
		}
		
		if(uebergewichtFrau  || uebergewichtMann){
				System.out.println("Klassifikation : Uebergewicht");
		}
		
	}

}
