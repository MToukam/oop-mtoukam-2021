import java.util.Scanner;

public class Tachenrechner {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine ganze Zahl ein:");
		int zahl1 = myScanner.nextInt();
		
		System.out.println("Bitte geben Sie eine zweite ganze Zahl ein:");
		int zahl2 = myScanner.nextInt();
		
		System.out.println("Wie m�chten Sie mit den beiden Zahlen operieren? Addieren, subtrahieren, multiplizieren, oder dividieren? Geben Sie bitte ein der folgenden Zeichen ein: +, - *, /");
		char operation = myScanner.next().charAt(0);
		
		switch (operation) {
		case '+':
			System.out.printf("Sie haben eine Addition ausgew�hlt, das Ergebnis ist : %d", zahl1+zahl2);
			break;
		
		case '-':
			System.out.printf("Sie haben eine Subtraktion ausgew�hlt, das Ergebnis ist : %d", zahl1-zahl2);
			break;
		
		case '*':
			System.out.printf("Sie haben eine Multiplikation ausgew�hlt, das Ergebnis ist : %d", zahl1*zahl2);
			break;
			
		case '/':
			System.out.printf("Sie haben eine Division ausgew�hlt, das Ergebnis ist : %.3f", (double) zahl1/zahl2);
			break;
			
		default:
			System.out.println("Sie haben ein falsches zeichen eingegeben, bitte geben eich richtiges Operatorzeichen ein");
			break;
			
		}
	

	}

}
