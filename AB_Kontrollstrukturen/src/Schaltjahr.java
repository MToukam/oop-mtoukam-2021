import java.util.Scanner;

public class Schaltjahr {

	public static void main(String[] args) {
		
		System.out.println("Sie werden nach einer Jahreszahl abgefragt, und danach wird ausgegeben, ob es sich um ein Schaltjahr handelt oder nicht!");
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine Jahreszahl ein: ");
		short jahr = myScanner.nextShort();
		
		String verb;
		if(jahr == 2021) {
			verb = "ist";
		}
		else {
			if(jahr < 2021) {
				verb = "war";
			}
			else {
				verb = "wird";
			}
		}
		
		boolean regel1 = (jahr % 4 == 0);			// jahr ganzzahlig durch 4 teilbar
		boolean regel2 = (jahr % 100 == 0);			// jahr ganzzahlig durch 100 teilbar
		boolean regel3 = (jahr % 400 == 0);			// jahr ganzzahlig durch 100 teilbar
		
		// Ist eine Jahreszahl durch 4 teilbar, ist es schonmal ein Schaltjahr, regel1 gilt.
		//Ausnahme: Jahreszahl ist durch 100 teilbar, dann ist es nicht mehr ein Schaltjahr, also muss gleichzeitig 
		//regel2 nicht gelten. Wenn aber regel2 gilt, ist es dann nur ein Schaltjahr, wenn auch regel3 gilt, also die Jahreszahl
		// durch 400 teilbar ist. Ansonston ist das kein Schaltjahr
		if( (regel1 && !regel2) || (regel2 && regel3)) {
			System.out.printf("Das Jahr %d %s ein Schaltjahr mit 366 Tagen!", jahr, verb);
		}
		else {
			System.out.printf("Das Jahr %d %s kein Schaltjahr!", jahr, verb);
		}

	}

}
