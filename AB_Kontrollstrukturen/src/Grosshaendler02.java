import java.util.Scanner;

public class Grosshaendler02 {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie bitte Ihren Bestellwert ein: ");
		int bestellwert = myScanner.nextInt();
		
		final double mwstr = 0.19;
		
		if((bestellwert <= 100) && (bestellwert > 0)){
			System.out.printf("Sie erhalten einen Rabatt von 10%% und Ihr  Bestellwert ist %.2f Euro incl. Mwstr", (0.90*bestellwert)*(1+mwstr));
		}
		else {
			if(bestellwert < 500) {
				System.out.printf("Sie erhalten einen Rabatt von 15%% und Ihr  Bestellwert ist %.2f Euro incl. Mwstr", (0.85*bestellwert)*(1+mwstr));
			} // der reduzierte Bestellwert wird mehrwertgesteuert
			else {
				System.out.printf("Sie erhalten einen Rabatt von 20%% und Ihr  Bestellwert ist %.2f Euro incl. Mwstr", (0.80*bestellwert)*(1+mwstr));
			}
		}
			
		

	}

}
