import java.util.Scanner;

public class Grosshaendler01 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie bitte die Anzahl der M�use ein: ");
		int anzahlMaeuse = myScanner.nextInt();
		
		System.out.println("Geben Sie bitte den Einzelpreis der M�use ein: ");
		int einzelpreis = myScanner.nextInt();
		
		if(anzahlMaeuse >= 10) {
			System.out.printf("Rechnungsbetrag incl.MwSt : %.2f Euro", 1.19*(anzahlMaeuse*einzelpreis));
		}			// mit 19% von Steuersatz
		else {
			System.out.printf("Rechnungsbetrag incl.MwSt : %.2f Euro", 1.19*(anzahlMaeuse*einzelpreis) + 10);
		}
		

	}

}
