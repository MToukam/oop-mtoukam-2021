import java.util.Scanner;

public class Aufgabe4 {

	public static void main(String[] args) {
		
		Scanner eingabe = new Scanner(System.in);
		System.out.println("Bitte den Startwert in Celsius eingeben: ");
		int start = eingabe.nextInt();
		
		System.out.println("Bitte den Endwert in Celsius eingeben: ");
		int endwert = eingabe.nextInt();
		
		System.out.println("Bitte die Schrittweite in Grad Celsius eingeben: ");
		int schritt = eingabe.nextInt();
		
		int i = start;
		double fa;
		
		
		while(i <= endwert) {
			fa = (1.8*i)  + 32;
			System.out.printf("%-1.2f%s  \t  %.2f�F%n", (double) i, "�C", fa);
			i += schritt;	
		}

	}

}
