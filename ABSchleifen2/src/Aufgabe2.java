import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		
		Scanner eingabe = new Scanner(System.in);
		System.out.println("Geben sie bitte eine natürliche Zahl  n <= 20 ein: ");
		int n = eingabe.nextInt();
		
		int i = 1, k = 1;
		
		if(n > 20) System.out.println("Falsche Eingabe");
		
		else if(n == 0) System.out.println(n + "! = " + "1");
		
		else {
			while(i <= n) {
				k *= i;
				i++;
			}
			System.out.print(n + "! = " + k);
		}
		

	}

}
