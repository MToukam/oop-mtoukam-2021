import java.util.Scanner;

public class Aufgabe1 {

	public static void main(String[] args) {
		
		Scanner eingabe = new Scanner(System.in);
		System.out.println("Geben sie bitte eine natürliche Zahl ein: ");
		int n = eingabe.nextInt();
	 
		// 1a
		int i = 1;
		while(i <= n) {
			if(i < n) System.out.print(i + ",");
			else System.out.println(i);
			i++;
		}
		
		// 1b
		i= n;
		while(i > 0) {
			if(i > 1) System.out.print(i + ",");
			else System.out.println(i);
			i--;
		}

	}

}
