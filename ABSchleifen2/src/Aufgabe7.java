
public class Aufgabe7 {

	public static void main(String[] args) {
		
		final double strecke = 1000.0;  // strecke in m
		final double gA = 9.5;			// Geschwindigkeit von A
		final double gB = 7;			// Geschwindigkeit von A
		
		int t_s = 1;					// Zeit in s
		int streckeA = 0;				// Strecke von A f�r 1s
		int streckeB = 0;				// Strecke von B f�r 1s
		String str = "";
		System.out.println("t/s     A       B");
		while((t_s <= (strecke/gA)) || (t_s <= (strecke/gB))) {
			streckeA = (int) (t_s*gA);
			streckeB = 250 + (int) (t_s*gB);
			if(t_s > (strecke/gA)) str = "A";
			else if ((t_s > (strecke/gB))) str = "B";
		
			if((t_s > (strecke/gA)) || (t_s > (strecke/gB))) {
				System.out.println("The WINNER ist: " + str);
				break;
			}
			
			System.out.printf("%-8d%dm%8dm%n", t_s, streckeA, streckeB);
			
			if(streckeA == streckeB) 
				System.out.println("A hat B nachgeholt"); 
			
			t_s++;
		}

	}

}
