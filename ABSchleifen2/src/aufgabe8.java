import java.util.Scanner;

public class aufgabe8 {

	public static void main(String[] args) {
		
		Scanner eingabe = new Scanner(System.in);
		System.out.println("Matrix\nBitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		int n = eingabe.nextInt();
		int i = 1;
		int j;
	
		String str = "";
		int[] quersumme = {0, 0, 0} ;
		
		while(i < 101) {
			
			if( (i % 10) == 0) str = "\n";
			else str = "";
			
			j = i-1;
			quersumme = bquersumme(j);
			
			boolean b1 = n==quersumme[0] || n==quersumme[1];
			
			if( (((i-1) % n)== 0 && i > n) || quersumme[2] == n || b1) {
				System.out.printf("%-4s%s", "*" , str);
			}
			else System.out.printf("%-4d%s", (i-1) , str);
			i++;
		}

	}
	
	public static int[] bquersumme(int qzahl) {
		int[] qsumme = new int[3];
		qsumme[0] = qzahl % 10;
		qzahl = qzahl/10;
		qsumme[1] = qzahl % 10;
		qsumme[2] = qsumme[0] + qsumme[1];
		return qsumme;
	}

}
