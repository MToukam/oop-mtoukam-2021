import java.util.Scanner;

public class Aufgabe1a {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte Grenze eingeben: ");
		int n =  tastatur.nextInt();
		
		for(int i = 1; i <= n; i++) {
			if(i < n) System.out.print(i + ", ");
			else System.out.print(i);
		}

	}

}
