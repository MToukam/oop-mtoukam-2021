
public class Aufgabe3 {

	public static void main(String[] args) {
		
		for(int i = 1; i <= 200; i++) {
			if((i%7) == 0) {
				System.out.printf("Die Zahl %-3d ist durch 7 teilbar.\n", i);
			}
			else if(((i%5) != 0) && ((i%4) == 0)) {
				System.out.printf("Die Zahl %-3d ist nicht  durch 5, aber durch 4 teilbar.\n", i);
			}
		}

	}

}
