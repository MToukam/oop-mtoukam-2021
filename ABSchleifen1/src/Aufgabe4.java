
public class Aufgabe4 {

	public static void main(String[] args) {
		// 4a
		for(int i = 99; i >= 9; i--) {
			if((i % 3) == 0) {
				if(i > 9) System.out.print(i+ ", ");
				else 	  System.out.println(i);
			}
		}
		// 4b
		for(int u = 1; u <= 20; u++) {
			for(int i = 1; i <= 20; i++) {
				if(u == i) {
					if(u < 20) System.out.print(u*i + ", ");
					else 	  System.out.println(u*i);
				}
			}
		} 
		
		// 4c
		for(int j = 1; j < 52; j = j+2) {
			if(j < 50) System.out.print(2*j + ", ");
			else 	  System.out.println(2*j);
		}
		
		// 4d
		for(int u = 2; u <= 32; u += 2) {
			for(int i = 2; i <= 32; i += 2) {
				if(u == i) {
					if(u < 32) System.out.print(u*i + ", ");
					else 	  System.out.println(u*i);
				}
			}
		} 
		
		// 4e
		for(int j = 1; j < 16; j++) {
			if(j < 15) System.out.print( (int) Math.pow(2, j) + ", ");
			else 	  System.out.println( (int) Math.pow(2, j));
		}


	}

}
