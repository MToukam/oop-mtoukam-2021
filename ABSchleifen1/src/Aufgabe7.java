
public class Aufgabe7 {

	public static void main(String[] args) {
		
		System.out.print("P = { 2, 3, 5, 7, ");
		
		for(int i = 1; i <= 100; i++) {
			if( ((i%2) != 0) && ((i%3) != 0) && ((i%5) != 0) && ((i%7) != 0) ) {
				if(i < 95) System.out.print(i + ", ");
				else System.out.print(i);
			}
		}
		
		System.out.println(" }");
		
		

	}

}
