import java.util.Scanner;

public class Aufgabe6 {

	public static void main(String[] args) {
		
		Scanner eingabe = new Scanner(System.in);
		System.out.println("Geben sie bitte eine natürliche Zahl ein: ");
		int n = eingabe.nextInt();
		String stern = "*";
		
		for(int i = 1; i <= n; i++) {
			System.out.printf("%s%n", stern);
			stern = stern + "*";
		}

	}

}
