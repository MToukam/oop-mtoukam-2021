import java.util.Scanner;

public class Aufgabe1b {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte Startwert eingeben: ");
		int n =  tastatur.nextInt();
		
		for(int i = n; i > 0; i--) {
			if(i > 1) System.out.print(i + ", ");
			else System.out.print(i);
		}


	}

}
