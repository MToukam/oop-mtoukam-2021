import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BuchTest {

	public static void main(String[] args) {
		
		List<Buch> buchliste = new ArrayList<Buch>();// ArrayList implementiert List, und List kann als Referenzfdatentyp verwendet werden
		buchliste.add(new Buch("Martin H.", "OOP", "12zH34"));
		buchliste.add(new Buch("Werner", "java", "123567Ik"));
		buchliste.add(new Buch("Marvel", "DB", "1aB236"));
		
		System.out.println(buchliste);
		
		Collections.sort(buchliste);    // Aussortieren: Buch mit dem kleinsten ISBN wird als erstes ausgegeben
	
		System.out.println(buchliste);  // Nach Aussortieren ausgeben

	}

}
