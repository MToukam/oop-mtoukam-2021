import java.util.*;

public class Hauptprogramm {

	public static void main(String[] args) {
		
		TreeSet<Buch> buchliste = new TreeSet<Buch>();
		buchliste.add(new Buch("Martin H.", "OOP", "12zH34"));
		buchliste.add(new Buch("Werner", "java", "123567Ik"));
		buchliste.add(new Buch("Marvel", "DB", "1aB236"));
		
		for (Iterator<Buch> it = buchliste.iterator(); it.hasNext();) {
			System.out.print(it.next() + " ");
		}
		
//		Ausgabe : [ Werner , java , 123567Ik] [ Martin H. , OOP , 12zH34] [ Marvel , DB , 1aB236] 
//		statt   : [ Martin H. , OOP , 12zH34] [ Werner , java , 123567Ik] [ Marvel , DB , 1aB236] 
//	    Man beobachtet, dass die Elemente der Buchliste in ansteigender Reihenfolge der ISBN aussortiert werden.
//		Es wird nach den ISBN aussortiert, weil die Methode comparTo in der klasse Buch implementiert ist, die Die ISBN von B�chern vergleicht. 
		

	}

}
