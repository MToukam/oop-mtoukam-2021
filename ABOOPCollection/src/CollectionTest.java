import java.util.*;

public class CollectionTest {
	
	public static void main(String[] args) {
		
		List<Buch> buchliste1 = new ArrayList<Buch>();// ArrayList implementiert List, und List kann als Referenzfdatentyp verwendet werden
		buchliste1.add(new Buch("Martin H.", "OOP", "12zH34"));
		buchliste1.add(new Buch("Werner", "java", "123567Ik"));
		buchliste1.add(new Buch("Marvel", "DB", "1aB236"));
		
		System.out.println(buchliste1);
		Buch b1 = buchAuffinden(buchliste1, "123567Ik");
		System.out.println(b1);
		
		buchEntfernen(buchliste1, 0);
		System.out.println(buchliste1);
		
		String isbn = gr��terISBN(buchliste1);
		System.out.println("Gr��ter ISBN der Buchliste  1: " + isbn);
		System.out.println(buchliste1);
		
		
		
		
		List<Buch> buchliste2 = new LinkedList<Buch>();
		buchliste2 = buchliste1;
		buchliste2.add(new Buch("Tiezter Schenke", "Analogelektronik", "ENU1236"));
		buchliste2.add(new Buch("Stefan G��ner", "Halbleiterelektronik", "EMIO3857"));
		
		System.out.println(buchliste2);
		Buch b2 = buchAuffinden(buchliste2, "EMIO3857");
		System.out.println(b2);
		System.out.println(buchAuffinden(buchliste2, "EMIO3T546"));
		
		buchEntfernen(buchliste2, 3);
		System.out.println(buchliste2);
		
		String isbn2 = gr��terISBN(buchliste2);
		System.out.println("Gr��ter ISBN der Buchliste 2: " + isbn2);
		
		
		
	}
	
	public static Buch buchAuffinden(List<Buch> buchliste, String isbn) {
		
//		for (Buch b1 : buchliste) {
//			if(isbn.equals( b1.getIsbn())) {
////				System.out.println("Buch [ Autor: "+ b1.getAutor() + ", Titel: " + b1.getTitel() + " ]");
//				return b1;
//			}
//		}
		
		for (Iterator<Buch> it = buchliste.iterator(); it.hasNext(); ) {
			Buch b1 = it.next();
			if(isbn.equals( b1.getIsbn())) {
				return b1;
			}
		}
		return null;
	}
	
	public static void buchEntfernen(List<Buch> buchliste, int index) {
		Buch b = buchliste.get(index);
		buchliste.remove(b);  // Methode remove der Collection-Klasse List
	}
	
	public static String gr��terISBN(List<Buch> buchliste) {
		Collections.sort(buchliste); // benutzt intern compareTo und dabei werden die ISBN jeweils vergleicht
		Buch b = buchliste.get(buchliste.size() - 1);
		return b.getIsbn();
	}
	
}
